<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// customer routes
Route::post('/customer', 'App\Http\Controllers\CustomerController@post');
Route::get('/customers', 'App\Http\Controllers\CustomerController@getAll');
Route::get('/customer/{id}', 'App\Http\Controllers\CustomerController@getOne');
Route::put('/customer/{id}', 'App\Http\Controllers\CustomerController@put');
Route::delete('/customer/{id}', 'App\Http\Controllers\CustomerController@delete');

// supplier routes
Route::post('/supplier', 'App\Http\Controllers\SupplierController@post');
Route::get('/suppliers', 'App\Http\Controllers\SupplierController@getAll');
Route::get('/supplier/{id}', 'App\Http\Controllers\SupplierController@getOne');
Route::put('/supplier/{id}', 'App\Http\Controllers\SupplierController@put');
Route::delete('/supplier/{id}', 'App\Http\Controllers\SupplierController@delete');

// product routes
Route::post('/product', 'App\Http\Controllers\ProductController@post');
Route::get('/products', 'App\Http\Controllers\ProductController@getAll');
Route::get('/product/{id}', 'App\Http\Controllers\ProductController@getOne');
Route::put('/product/{id}', 'App\Http\Controllers\ProductController@put');
Route::delete('/product/{id}', 'App\Http\Controllers\ProductController@delete');

// transaction routes
Route::post('/transaction', 'App\Http\Controllers\TransactionController@post');
Route::get('/transactions', 'App\Http\Controllers\TransactionController@getAll');
Route::get('/transaction/{id}', 'App\Http\Controllers\TransactionController@getOne');
Route::put('/transaction/{id}', 'App\Http\Controllers\TransactionController@put');
Route::delete('/transaction/{id}', 'App\Http\Controllers\TransactionController@delete');
