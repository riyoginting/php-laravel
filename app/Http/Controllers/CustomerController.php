<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use Eloquent
use App\Models\Customer;

class CustomerController extends Controller
{
    function post(Request $request)
    {
        // create new customer
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->phone_number = $request->phone_number;

        // save to database
        $customer->save();

        // send response
        return response()->json(
            [
                "message" => "Success",
                "data" => $customer
            ]
        );
    }

    function getAll()
    {
        // get all data from db
        $data = Customer::all();

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function getOne($id)
    {
        // get one data from db
        $data = Customer::where('id', $id)->get();

        // if data is not found
        if (count($data) == 0) {
            return response()->json(
                [
                    "message" => "Customer with id $id not found"
                ],
                404
            );
        }

        // if success
        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function put($id, Request $request)
    {
        // find data by id to update
        $customer = Customer::where('id', $id)->first();

        // if data does not exist
        if (!$customer) {
            return response()->json(
                [
                    "message" => "Customer with id $id not found"
                ],
                404
            );
        }

        // set value to be update
        // give validation incase user does not give some data, so the old value will be use
        $customer->name = $request->name ? $request->name : $customer->name;
        $customer->address = $request->address ? $request->address : $customer->address;
        $customer->phone_number = $request->phone_number ?
            $request->phone_number : $customer->phone_number;

        // save updated product to db
        $customer->save();

        // send response
        return response()->json(
            [
                "message" => "Success ",
                "data" => $customer
            ]
        );
    }

    function delete($id)
    {
        // find customer
        $customer = Customer::where('id', $id)->first();

        // if customer is not found
        if (!$customer) {
            return response()->json(
                [
                    "message" => "customer with id $id not found"
                ],
                404
            );
        }

        // if customer exist, delete it
        $customer->delete();

        return response()->json(
            [
                "message" => "customer with id $id successfully deleted"
            ]
        );
    }
}
