<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;

class SupplierController extends Controller
{
    function post(Request $request)
    {
        $supplier = new Supplier;
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->phone_number = $request->phone_number;

        $supplier->save();

        return response()->json(
            [
                "message" => "Success",
                "data" => $supplier
            ]
        );
    }

    function getAll()
    {
        $data = Supplier::all();

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function getOne($id)
    {
        $data = Supplier::where('id', $id)->get();

        if (count($data) == 0) {
            return response()->json(
                [
                    "message" => "Supplier with id $id not found"
                ],
                404
            );
        }

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function put($id, Request $request)
    {
        $supplier = Supplier::where('id', $id)->first();

        if (!$supplier) {
            return response()->json(
                [
                    "message" => "supplier with id $id not found"
                ],
                404
            );
        }

        $supplier->name = $request->name ? $request->name : $supplier->name;
        $supplier->address = $request->address ? $request->address : $supplier->address;
        $supplier->phone_number = $request->phone_number ?
            $request->phone_number : $supplier->phone_number;

        $supplier->save();

        return response()->json(
            [
                "message" => "Success ",
                "data" => $supplier
            ]
        );
    }

    function delete($id)
    {
        $supplier = Supplier::where('id', $id)->first();

        if (!$supplier) {
            return response()->json(
                [
                    "message" => "Supplier with id $id not found"
                ],
                404
            );
        }

        $supplier->delete();

        return response()->json(
            [
                "message" => "Supplier with id $id successfully deleted"
            ]
        );
    }
}
