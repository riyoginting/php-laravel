<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Supplier;

class ProductController extends Controller
{
    function post(Request $request)
    {
        // cek request supplier_id valid or not
        $supplier = Supplier::where('id', $request->supplier_id)->get();

        // if supplier not found
        if (count($supplier) == 0) {
            return response()->json(
                [
                    "message" => "Supplier with id $request->supplier_id not found"
                ],
                400
            );
        }

        $product = new Product;
        $product->supplier_id = $request->supplier_id;
        $product->name = $request->name;
        $product->price = $request->price;

        $product->save();

        return response()->json(
            [
                "message" => "Success",
                "data" => $product
            ]
        );
    }

    function getAll()
    {
        $data = Product::all();

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function getOne($id)
    {
        $data = Product::where('id', $id)->get();

        if (count($data) == 0) {
            return response()->json(
                [
                    "message" => "Product with id $id not found"
                ],
                404
            );
        }

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function put($id, Request $request)
    {
        $product = Product::where('id', $id)->first();

        if (!$product) {
            return response()->json(
                [
                    "message" => "Product with id $id not found"
                ],
                404
            );
        }

        // cek request supplier_id valid or not
        if ($request->supplier_id) {
            $supplier = Supplier::where('id', $request->supplier_id)->get();

            // if supplier not found
            if (count($supplier) == 0) {
                return response()->json(
                    [
                        "message" => "Supplier with id $request->supplier_id not found"
                    ],
                    404
                );
            }
        }


        $product->supplier_id = $request->supplier_id ? $request->supplier_id : $product->supplier_id;
        $product->name = $request->name ? $request->name : $product->name;
        $product->price = $request->price ? $request->price : $product->price;

        $product->save();

        return response()->json(
            [
                "message" => "Success ",
                "data" => $product
            ]
        );
    }

    function delete($id)
    {
        $product = Product::where('id', $id)->first();

        if (!$product) {
            return response()->json(
                [
                    "message" => "Product with id $id not found"
                ],
                404
            );
        }

        $product->delete();

        return response()->json(
            [
                "message" => "Product with id $id successfully deleted"
            ]
        );
    }
}
