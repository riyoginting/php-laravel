<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\Customer;

class TransactionController extends Controller
{
    function post(Request $request)
    {
        // count total price
        $data = Product::where('id', $request->product_id)->get();
        $request->total_price = $data[0]['price'] * $request->total_item;

        $transaction = new Transaction;
        $transaction->product_id = $request->product_id;
        $transaction->customer_id = $request->customer_id;
        $transaction->total_item = $request->total_item;
        $transaction->total_price = $request->total_price;

        $transaction->save();

        return response()->json(
            [
                "message" => "Success",
                "data" => $transaction
            ]
        );
    }

    function getAll()
    {
        $data = Transaction::all();

        if (count($data) == 0) {
            return response()->json(
                [
                    "message" => "Data is empty"
                ],
                404
            );
        }

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function getOne($id)
    {
        $data = Transaction::where('id', $id)->get();

        if (count($data) == 0) {
            return response()->json(
                [
                    "message" => "Trasaction with id $id not found"
                ],
                404
            );
        }

        return response()->json(
            [
                "message" => "Success",
                "data" => $data
            ]
        );
    }

    function put($id, Request $request)
    {
        $transaction = Transaction::where('id', $id)->first();

        if (!$transaction) {
            return response()->json(
                [
                    "message" => "Transaction with id $id not found"
                ],
                404
            );
        }

        // cek customer_id valid or not
        if ($request->customer_id) {
            $customer = customer::where('id', $request->customer_id)->get();

            // if customer not found
            if (count($customer) == 0) {
                return response()->json(
                    [
                        "message" => "customer with id $request->customer_id not found"
                    ],
                    404
                );
            }
        }

        // cek product_id valid or not
        if ($request->product_id) {
            $product = Product::where('id', $request->product_id)->get();

            // if product not found
            if (count($product) == 0) {
                return response()->json(
                    [
                        "message" => "Product with id $request->product_id not found"
                    ],
                    404
                );
            }
        }

        // count total price if total item update
        if ($request->total_item) {
            $request->total_price = $request->total_item * $product[0]['price'];
        }


        $transaction->product_id = $request->product_id ?
            $request->product_id : $transaction->product_id;

        $transaction->customer_id = $request->customer_id ?
            $request->customer_id : $transaction->customer_id;

        $transaction->total_item = $request->total_item ?
            $request->total_item : $transaction->total_item;

        $transaction->total_price = $request->total_price ?
            $request->total_price : $transaction->total_price;

        $transaction->save();

        return response()->json(
            [
                "message" => "Success ",
                "data" => $transaction
            ]
        );
    }

    function delete($id)
    {
        $transaction = Transaction::where('id', $id)->first();

        if (!$transaction) {
            return response()->json(
                [
                    "message" => "Transaction with id $id not found"
                ],
                404
            );
        }

        $transaction->delete();

        return response()->json(
            [
                "message" => "Transaction with id $id successfully deleted"
            ]
        );
    }
}
